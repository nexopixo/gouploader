# Compiling
FROM golang:latest AS gobuilder
LABEL maintainer = "André Jacques <andre.jacques@nexopixo.com>"
RUN go get -d -v github.com/gorilla/mux
WORKDIR /home/coderslife/goes/src/bitbucket.org/nexopixo/gouploader
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o gouploader .

# Running
FROM alpine:latest
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=0 /home/coderslife/goes/src/bitbucket.org/nexopixo/gouploader .
EXPOSE 8080
CMD ["./gouploader"]